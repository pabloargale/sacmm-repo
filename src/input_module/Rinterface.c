#include "AMIGO_model.h"
#include "AMIGO_problem.h"
#include "AMIGO_pe.h"
#include <stdlib.h>
#include <Rinternals.h>
#include <string.h>
#include <Rembedded.h>
#include <Rdefines.h>

void load_source(const char *name)
{
    SEXP e;
 
    PROTECT(e = lang2(install("source"), mkString(name)));
    R_tryEval(e, R_GlobalEnv, NULL);
    UNPROTECT(1);
}


int getListElement (SEXP list, char *str){
  
  SEXP names = getAttrib(list, R_NamesSymbol);
  int i;
  
  for (i = 0; i < length(list); i++)
    if(strcmp(CHAR(STRING_ELT(names, i)), str) == 0) {
      return(i);
    }

    return(-1);
}
 

AMIGO_model * RAMIGOmodelAlloc(SEXP model, SEXP exps, SEXP ivpsol, int exp_num){
  AMIGO_model *amigo_model;
  SEXP exp;
  int elm1,n_boot;
  int n_states,n_observables,n_pars,n_opt_pars,n_times,n_opt_ics,n_controls,n_controls_t,i,j,counter;  


  exp=VECTOR_ELT(exps, exp_num);

  elm1=getListElement(model,"n_states");
  if(elm1>=0) n_states=INTEGER(VECTOR_ELT(model,elm1))[0];
  else error("ERROR");

  elm1=getListElement(exp,"n_observables");
  if(elm1>=0) n_observables=INTEGER(VECTOR_ELT(exp,elm1))[0];
  else error("ERROR");
  
  elm1=getListElement(model,"x"); 
  if(elm1>=0) n_pars=length(VECTOR_ELT(model,elm1));
  else error("ERROR");
  
  elm1=getListElement(exp,"t_s");
  if(elm1>=0) n_times=length(VECTOR_ELT(exp,elm1));
  else error("ERROR");
 
  elm1=getListElement(model,"n_stimuli");
  if(elm1>=0) n_controls=INTEGER(VECTOR_ELT(model,elm1))[0];
  else error("ERROR");

  elm1=getListElement(exp,"t_con");
  if(elm1>=0)  n_controls_t=length(VECTOR_ELT(exp,elm1));
  else error("ERROR");

  n_opt_pars=0; 
  n_opt_ics=0;

  amigo_model=allocate_AMIGO_model(n_states,n_observables,n_pars,
   	n_opt_pars,n_times,n_opt_ics,n_controls, n_controls_t,exp_num);

//  elm1=getListElement(model,"bootstrap");
//  if(elm1>=0) {
//        n_boot=length(VECTOR_ELT(model,elm1));
//        (amigo_model)->bootstrap = (double *) malloc(n_boot*sizeof(double));
//  }
//  else n_boot=0;
//  (amigo_model)->n_boot = n_boot;

//  if (n_boot > 0 ) {
//  	elm1=getListElement(model,"bootstrap");
//  	for (i = 0; i < n_boot; i++){
//  	  (amigo_model)->bootstrap[i]= ((double) INTEGER(VECTOR_ELT(model,elm1))[i]-1);
//  	}
//  }

  
 //index_observables
  elm1=getListElement(exp,"index_observables");
  if(elm1>=0) ;
  else error("ERROR field index_observables must exist");
  if(length(VECTOR_ELT(exp,elm1))<n_observables){
		(amigo_model)->use_obs_func=1;
		(amigo_model)->use_sens_obs_func=1;
  }else{
    for (i = 0; i < length(VECTOR_ELT(exp,elm1)); i++)
      (amigo_model)->index_observables[i]=INTEGER(VECTOR_ELT(exp,elm1))[i]-1;
   (amigo_model)->use_obs_func=0;
   (amigo_model)->use_sens_obs_func=0;

  }

  //Simulation Pars
  elm1=getListElement(model,"x");
  if(elm1>=0);
  else error("ERROR field pars ust exist in model");
  for (i = 0; i < n_pars; i++){
		(amigo_model)->pars[i]=REAL(VECTOR_ELT(model,elm1))[i];
  }
 
    
  //initial simulation times
  elm1=getListElement(exp,"t_0");
  if(elm1>=0) ;
  else error("ERROR field t_0 must exist in experimental definition");
	(amigo_model)->t0=REAL(VECTOR_ELT(exp,elm1))[0];

  //initial simulation times
  elm1=getListElement(exp,"t_f");
  if(elm1>=0) ;
  else error("ERROR field t_f must exist in experimental definition");
	(amigo_model)->tf=REAL(VECTOR_ELT(exp,elm1))[0];
  
  //Sampling times
  elm1=getListElement(exp,"t_s");
  if(elm1>=0) ;
  else error("ERROR field t_s must exist in experimental definition");
	for (i = 0; i < n_times; i++){
		(amigo_model)->t[i]=REAL(VECTOR_ELT(exp,elm1))[i];
	}
  
  //Initial conditions
  elm1=getListElement(exp,"y0");
  if(elm1>=0) ;
  else error("ERROR field y0 must exist in experimental definition");
  for (i = 0; i < n_states; i++){
		(amigo_model)->y0[i]=REAL(VECTOR_ELT(exp,elm1))[i];
  }
   
   //Control times	
  elm1=getListElement(exp,"t_con");
  if(elm1>=0) ;
  else error("ERROR field t_con must exist in experimental definition");
	for (i = 0; i <n_controls_t; i++){
		(amigo_model)->controls_t[i]=REAL(VECTOR_ELT(exp,elm1))[i];
	}
  
  //Control values
  counter=0;
  elm1=getListElement(exp,"u");
  if(elm1>=0) ;
  else error("ERROR field u must exist in experimental definition");
	for (i = 0; i < n_controls; i++) {
		for (j= 0; j < n_controls_t-1; j++){
			(amigo_model)->controls_v[i][j]=REAL(VECTOR_ELT(exp,elm1))[counter++];

		}
	}

 //Experimental data
  counter=0;
  elm1=getListElement(exp,"exp_data");
  if(elm1>=0) ;
  else error("Experimental data  field must exist in experimental definition");
	if (n_observables>0){
		for (i = 0; i < n_observables; i++){
			for (j = 0; j < n_times; j++){
				(amigo_model)->exp_data[i][j]=REAL(VECTOR_ELT(exp,elm1))[counter++];
			}
		}
  }
  
  //Simulation Related Parameter
  //rtol
  elm1=getListElement(ivpsol,"rtol");
  if(elm1>=0) ;
  else error("rtol field must exist in experimental definition");
  (amigo_model)->reltol=REAL(VECTOR_ELT(ivpsol,elm1))[0];
  
  //atol
  elm1=getListElement(ivpsol,"atol");
  if(elm1>=0) ;
  else error("atol field must exist in experimental definition");
  (amigo_model)->atol=REAL(VECTOR_ELT(ivpsol,elm1))[0];

  //max_step_size
  elm1=getListElement(ivpsol,"max_step_size");
  if(elm1>=0) ;
  else error("max_step_size field must exist in experimental definition");
  (amigo_model)->max_step_size=REAL(VECTOR_ELT(ivpsol,elm1))[0];

  //printf("max_step_size %lf\n", (amigo_model)->max_step_size);
  //max_num_steps
  elm1=getListElement(ivpsol,"max_num_steps");
  if(elm1>=0) ;
  else error("max_num_steps field must exist in experimental definition");
  (amigo_model)->max_num_steps=INTEGER(VECTOR_ELT(ivpsol,elm1))[0];
  //printf("max_num_steps %d\n", (amigo_model)->max_num_steps);

  //max_error_test_fails
  elm1=getListElement(ivpsol,"max_error_test_fails");
  if(elm1>=0) ;
  else error("max_error_test_fails field must exist in experimental definition");
  (amigo_model)->max_error_test_fails=INTEGER(VECTOR_ELT(ivpsol,elm1))[0];
  //printf("max_error_test_fails %d\n", (amigo_model)->max_error_test_fails);
 

  return amigo_model;
}

void extract_param(SEXP model,  SEXP exps, AMIGO_problem *amigo_problem) {
    int elm1, i, counter, n;  
    SEXP exp;
    counter=0;

    elm1=getListElement(model,"x");
    if(elm1>=0) n=length(VECTOR_ELT(model,elm1));
    else error("ERROR x");
    amigo_problem->n_pars=n;
    elm1=getListElement(model,"k_pars");
    if(elm1>=0) amigo_problem->n_k_pars=length(VECTOR_ELT(model,elm1));
    else error("ERROR k_pars");
    elm1=getListElement(model,"n_pars");
    if(elm1>=0) amigo_problem->n_n_pars=length(VECTOR_ELT(model,elm1));
    else error("ERROR n_pars");
    elm1=getListElement(model,"w_pars");
    if(elm1>=0) amigo_problem->n_w_pars=length(VECTOR_ELT(model,elm1));
    else error("ERROR w_pars");
    elm1=getListElement(model,"tau_pars");
    if(elm1>=0) amigo_problem->n_tau_pars=length(VECTOR_ELT(model,elm1));
    else error("ERROR tau_pars");
 
    amigo_problem->k_pars_index =   (int *) malloc(amigo_problem->n_k_pars*sizeof(int));
    amigo_problem->n_pars_index =   (int *) malloc(amigo_problem->n_n_pars*sizeof(int));
    amigo_problem->w_pars_index =   (int *) malloc(amigo_problem->n_w_pars*sizeof(int));
    amigo_problem->tau_pars_index = (int *) malloc(amigo_problem->n_tau_pars*sizeof(int));

    elm1=getListElement(model,"k_pars");
    if(elm1>=0);
    else error("ERROR field k_pars ust exist in model");
    for (i = 0; i < amigo_problem->n_k_pars; i++){
       amigo_problem->k_pars_index[i]= ((int) INTEGER(VECTOR_ELT(model,elm1))[i]-1);
       counter++;
    }

    elm1=getListElement(model,"n_pars");
    if(elm1>=0);
    else error("ERROR field n_pars ust exist in model");
    for (i = 0; i < amigo_problem->n_n_pars; i++){
       amigo_problem->n_pars_index[i]= ((int) INTEGER(VECTOR_ELT(model,elm1))[i]-1);
       counter++;
    }

    elm1=getListElement(model,"w_pars");
    if(elm1>=0);
    else error("ERROR field w_pars ust exist in model");
    for (i = 0; i <  amigo_problem->n_w_pars; i++){
       amigo_problem->w_pars_index[i]= ((int) INTEGER(VECTOR_ELT(model,elm1))[i]-1);
       counter++;
    }

    elm1=getListElement(model,"tau_pars");
    if(elm1>=0);
    else error("ERROR field tau_pars ust exist in model");
    for (i = 0; i < amigo_problem->n_tau_pars; i++){
       amigo_problem->tau_pars_index[i]= ((int) INTEGER(VECTOR_ELT(model,elm1))[i]-1);
       counter++;
    }

    elm1=getListElement(model,"index_inputs");
    if(elm1>=0) amigo_problem->size_index_inputs=length(VECTOR_ELT(model,elm1));
    else error("ERROR index_inputs");

    elm1=getListElement(model,"index_inputs");
    if(elm1>=0);
    else error("ERROR field index_inputs ust exist in model");
    amigo_problem->index_inputs = ( int *) malloc( amigo_problem->size_index_inputs * sizeof(int));
    for (i = 0; i < amigo_problem->size_index_inputs; i++){
       amigo_problem->index_inputs[i]=((int) REAL(VECTOR_ELT(model,elm1))[i]-1); 
    }

    amigo_problem->is_stimuli_n=amigo_problem->amigo_models[0]->n_controls;

    exp=VECTOR_ELT(exps, 0);
    elm1=getListElement(exp,"is_stimuli");
    if(elm1>=0);
    else error("ERROR field is_stimuli must exist in model");
    amigo_problem->is_stimuli = (int *) malloc( amigo_problem->is_stimuli_n *sizeof(int));
    for (i = 0; i < amigo_problem->is_stimuli_n; i++){
               amigo_problem->is_stimuli[i]=((int) REAL(VECTOR_ELT(exp,elm1))[i]);
    }


}


AMIGO_problem* openRFileAMIGO(const char *string){
    AMIGO_problem *amigo_problem;
    AMIGO_model **amigo_models;
    int elm1,nx,n_opt_pars;
    SEXP model, exps, ivpsol, r_alpha_num,r_exp_num, r_pes, ret, r_n_data;
    SEXP model_j, exps_j;
    SEXP arg;
    int n_data,i;
    double alpha_num;
    int r_argc = 4;
    char *r_argv[] = { "R", "--gui=none", "--silent", "--no-save"};

    int exp_num, model_num;
    double pes;
    SEXP DREAMBT20;
    int errorOccurred;
    int *par;

    par = (int *) malloc(sizeof(int));
   
 
    Rf_initEmbeddedR(r_argc, r_argv);
    load_source(string);
    par[0]=1;
    PROTECT(arg = allocVector(INTSXP, 1));
    memcpy(INTEGER(arg), par, sizeof(int));
    PROTECT(DREAMBT20 = lang2(install("create_R_DREAMBT20_problem"), arg));
    PROTECT(ret = R_tryEval(DREAMBT20, R_GlobalEnv, &errorOccurred));

    if (!errorOccurred){
            ivpsol = VECTOR_ELT(ret, 0);
            model =  VECTOR_ELT(ret, 1);
            exps =   VECTOR_ELT(ret, 2);
	    r_exp_num = VECTOR_ELT(ret,3);
            r_n_data =   VECTOR_ELT(ret,4);
            exp_num =  asInteger(r_exp_num);
	    n_data =   asInteger(r_n_data);
    }
    else
    {
            printf("Error occurred calling R\n");
    }

    UNPROTECT(3);
    Rf_endEmbeddedR(0);
	
    amigo_problem =(AMIGO_problem*)malloc(sizeof(AMIGO_problem));
    amigo_models  = (AMIGO_model**)malloc(sizeof(AMIGO_model *)*exp_num);
    elm1=getListElement(model,"UB");
    if(elm1>=0)  nx=length(VECTOR_ELT(model,elm1));
    else error("ERROR UB");

    for (i=0;i<exp_num;i++) {
	  amigo_models[i] = RAMIGOmodelAlloc( model, exps, ivpsol, i);
    }
    amigo_problem = allocate_AMIGO_problem( exp_num, amigo_models);

    elm1=getListElement(model,"index_opt");
    if(elm1>=0) n_opt_pars=length(VECTOR_ELT(model,elm1));
    else error("ERROR field index_opt ust exist in model");
    amigo_problem->nx = n_opt_pars;

    amigo_problem->index_opt = (int *) malloc(n_opt_pars*sizeof(int));

    elm1=getListElement(model,"index_opt");
    if(elm1>=0);
    else error("ERROR field index_opt ust exist in model");
    for (i = 0; i < n_opt_pars; i++){
       amigo_problem->index_opt[i]= ((int) REAL(VECTOR_ELT(model,elm1))[i]-1);
    }

    extract_param(model, exps, amigo_problem); 
    amigo_problem->n_exp = exp_num;
    amigo_problem->n_models = exp_num;
    amigo_problem->n_dataR= n_data;
    amigo_problem->nx=n_opt_pars; 
    return amigo_problem;
}




void get_reduced_model(AMIGO_problem *amigo, int delete_edge) {
    int counter,count_inputs,i,j,k;
    int n_inputs, is_active;
    int **active_inputs, *active_inputs_aux_ite, *sizes_active_inputs;
    double *x;

    active_inputs = (int **) malloc(amigo->is_stimuli_n*sizeof(int *));
    sizes_active_inputs = (int *) malloc(amigo->is_stimuli_n*sizeof(int));
    for (i=0; i < amigo->is_stimuli_n; i++) {
		active_inputs[i] = (int *) malloc(sizeof(int));
		sizes_active_inputs[i] = 0;
    }

    free(amigo->index_inputs);
    amigo->index_inputs = NULL;
    amigo->index_inputs = (int *) malloc(1*sizeof(int));
	
    x = (double *) malloc ( amigo->n_pars * sizeof(double));

    for (j = 0;  j < amigo->amigo_models[0]->n_pars; j++){
         x[j] =  amigo->amigo_models[0]->pars[j];
    }

    amigo->size_index_inputs = 0;
    amigo->count_inputs=0;
    counter=-1;	



    for (i=0; i < amigo->is_stimuli_n; i++) {
	     
		 counter = counter + 1;		 
		 n_inputs= (int) x[counter];
		 if (n_inputs > 0 ) {
			for (j=0;j<n_inputs; j++) {
				// inputs index
				counter = counter + 1;
                                amigo->count_inputs = amigo->count_inputs + 1;
				if (delete_edge == amigo->count_inputs) {
					x[counter] = -1.0;
				}
				
				if(amigo->size_index_inputs != 0) {
					amigo->index_inputs = (int *) realloc(amigo->index_inputs,(amigo->size_index_inputs+1)*sizeof(int));
				}
                                amigo->index_inputs[amigo->size_index_inputs]=counter;
				amigo->size_index_inputs++;
				
				is_active = (int) x[counter];
				if (is_active >=0 ) {
					if (sizes_active_inputs[i] != 0) {
						active_inputs[i] = (int *) realloc(active_inputs[i],(sizes_active_inputs[i]+1)*sizeof(int));
					}
                                        active_inputs[i][sizes_active_inputs[i]]=is_active;
					sizes_active_inputs[i]++;
				}
				
				//k
				counter=counter+1;
				
				//n
				counter=counter+1;
			}
			
			//w
			for (j=0;j<((int)pow(2,n_inputs));j++){
				counter=counter+1;
			}
			
			//TAU
			counter=counter+1;
			
		 } else {
			counter= counter +1;
		 }
	}
	amigo->estimated_pars=amigo->nx;
   	for (i = 0;  i < amigo->n_models; i++){
        	 for (j = 0;  j < amigo->n_pars; j++){
        	       amigo->amigo_models[i]->pars[j]=x[j];
        	 }
    	}

      	
	amigo->count_active_inputs = 0;
	amigo->active_pars = 0;
	for (i=0;i<amigo->is_stimuli_n;i++) {
		n_inputs=sizes_active_inputs[i];
		if ( n_inputs > 0 ) {
			amigo->count_active_inputs = amigo->count_active_inputs+n_inputs;
			amigo->active_pars = amigo->active_pars + ( (int) ( 2.0 * n_inputs +  pow(2.0,n_inputs) + 1));
		} else if ( (n_inputs==0) && (  amigo->is_stimuli[i] == 0 )) {
			amigo->active_pars=amigo->active_pars+1;
		}
	}

        
	free(x);
	free(sizes_active_inputs);
	for (i=0;i<amigo->is_stimuli_n;i++){
		free(active_inputs[i]);
	}
	free(active_inputs);

}
