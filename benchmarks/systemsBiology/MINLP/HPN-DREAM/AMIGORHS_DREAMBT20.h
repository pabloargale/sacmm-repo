#include <stdlib.h>
#include <string.h>
#include <AMIGO_model.h>
#include <AMIGO_problem.h>
#include <amigoRHS.h>
//#define inhibitor(i)



/* Right hand side of the system (f(t,x,p))*/

int amigoRHS_DREAMBT20(realtype , N_Vector , N_Vector , void *);

void amigoRHS_get_sens_OBS_DREAMBT20(void* );

void amigoRHS_get_OBS_DREAMBT20(void* );

void amigo_Y_at_tcon_DREAMBT20(void* , realtype , N_Vector );

double fitnessfunction_DREAMBC(double *, void * );
