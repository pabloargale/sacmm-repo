getRandomLBODEmodel <- function(adjMat,maxInput){ 
  
  
  tempMat=replicate(dim(adjMat)[1],runif(dim(adjMat)[1]))
  adjMat=adjMat+tempMat*0.001;
  print('I do get here')
  
  for (i in 1:dim(adjMat)[1]){
    
    col_adjMat=adjMat[,i];
    
    norm_col_adjMat=col_adjMat/sum(col_adjMat);
    
    temp=c();
    
    while (length(temp)<maxInput){
      
      random_number=runif(1);
      temp_sum=0;
      
      for (j in 1:length(norm_col_adjMat)){
        
        temp_sum=temp_sum+norm_col_adjMat[j];
        
        if(temp_sum>=random_number){
          temp=unique(c(temp,j));
          break;
        }
      }
    }
    
    adjMat[,i]=matrix(0,1,dim(adjMat)[1]);
    
    for (j in 1:maxInput){
      adjMat[temp[j],i]=1;
    }
    
  }
  
  return(adjMat);
  
}
