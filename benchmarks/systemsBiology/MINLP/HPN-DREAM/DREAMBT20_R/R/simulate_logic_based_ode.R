simulate_logic_based_ode <- function(model,exps,ivpsol){   
  
  task="sim_CVODES_ODE";
  
  res=.Call("sim_logic_ode",model,exps,ivpsol,task);
  
  return(res);
  
}

