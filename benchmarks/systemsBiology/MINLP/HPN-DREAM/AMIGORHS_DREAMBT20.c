#include <stdlib.h>
#include <string.h>
#include <AMIGO_model.h>
#include <AMIGO_problem.h>
#include <amigoRHS.h>
#include <structure_paralleltestbed.h>



//#define inhibitor(i)



/* Right hand side of the system (f(t,x,p))*/
int amigoRHS_DREAMBT20(realtype t, N_Vector y, N_Vector ydot, void *data){
    AMIGO_model* amigo_model=(AMIGO_model*)data;
    
    
    int n_states, n_inputs;
    
    double inhibitor,x,k,n,w,tau;
    int i,j,m,n_minterms;
    int ind,ind2;
    int counter=0;
    int input;
    int dec;
    int countBin=0;
    int vecBin[300];
    double resprod;
    double ressum;
    
    
    double fHill[30];
    
    
    n_states=amigo_model->n_states;
    
    for(i = 0; i < n_states; i++){
        //
        n_inputs=(int)amigo_model->pars[counter++];
        if(n_inputs>0){
            
            for(j = 0; j < n_inputs; j++){
                
                input=(int)amigo_model->pars[counter++];
                k=amigo_model->pars[counter++];
                n=amigo_model->pars[counter++];
                
                if(input==-1){
                    fHill[j]=1;
                }else if(input==-2){
                    fHill[j]=0;
                }else{
					
					      inhibitor=(*amigo_model).controls_v[input][0];
                //if(inhibitor>0){
                //  Rprintf("input %d %f\n",input+1,inhibitor);
                //}
                
                x=Ith(y,input)*(1-inhibitor);
                fHill[j]=min(1,max(0,(pow(x,n)/(pow(x,n)+pow(k,n)))*(1+pow(k,n))));
					
              }
            }
            
            ressum=0;
            n_minterms=(int)pow(2,n_inputs);
            
            for(ind = 0; ind < n_minterms; ind++){
                
                dec=ind;
                resprod=1;
                
                for(m=n_inputs-1;m>=0;m--){
                    vecBin[m]=(int)dec%2;
                    dec=(int)dec/2;
                
                }
                
                for(ind2 = 0; ind2 < n_inputs; ind2++){
                    
                    if(vecBin[ind2]){
                        resprod*=fHill[ind2];
                    }else{
                        resprod*=(1-fHill[ind2]);
                    }
                
                    
                }
                //mexPrintf("\n");
                //mexPrintf("\nind=%d\n",ind);
                
                w=amigo_model->pars[counter++];
                ///mexPrintf("w=%e\n",w);
                ressum+=resprod*w;
                
            }
         
            tau=amigo_model->pars[counter++];
            
            inhibitor=(*amigo_model).controls_v[i][0];
            //if(inhibitor>0)Rprintf("Exp n=%d state_num=%d  inhhibitor=%f\n",amigo_model->exp_num,i,inhibitor);
            //Rprintf("Exp n=%d state_num=%d  inhhibitor=%f\n",amigo_model->exp_num,i,inhibitor);
            //Ith(ydot,i)=(ressum-Ith(y,i))*tau*(1-inhibitor);
			Ith(ydot,i)=(ressum-Ith(y,i))*tau;
            
            
        }else{
            tau=amigo_model->pars[counter++];
            Ith(ydot,i)=( 0 - Ith(y,i) )*tau;
            
        }
        
    }
    

    return(0);
    
}
void amigoRHS_get_sens_OBS_DREAMBT20(void* data){
    
}

void amigoRHS_get_OBS_DREAMBT20(void* data){
    
}

void amigo_Y_at_tcon_DREAMBT20(void* data, realtype t, N_Vector y){
    AMIGO_model* amigo_model=(AMIGO_model*)data;
    
}

double fitnessfunction_DREAMBC(double *x, void * data){
    experiment_total *exp1;
    AMIGO_problem* amigo_problem;
    int *vars_to_reduce;
    int delete_edge;
    int negativo, counter_vars_to_reduce, counter_k;
    double res, res2, RMSE;
    exp1 = (experiment_total *) data;
    amigo_problem= exp1->amigo;
    int k, j, i;
    int counter_kk;
    double **x_pars;
    
    

    // COPIA ORIXINAL
    x_pars = (double **) malloc( amigo_problem->n_models * sizeof(double *) );
    for (i = 0;  i < amigo_problem->n_models; i++){
	x_pars[i] = (double *) malloc ( amigo_problem->n_pars * sizeof(double));
	for ( j = 0; j < amigo_problem->n_pars; j++) {
		x_pars[i][j]=amigo_problem->amigo_models[i]->pars[j];
	}
    }
   
   for (k = 0;  k < amigo_problem->n_models; k++){
         for (j = 0;  j < amigo_problem->nx ; j++){
               amigo_problem->amigo_models[k]->pars[amigo_problem->index_opt[j]]=x[j];
	       
         }
    }

    counter_vars_to_reduce=0;	

    for (k = amigo_problem->nx; k < exp1->test.bench.dim; k++){
	if (x[k]==1) {
		counter_vars_to_reduce++;
	}
    }
    if (counter_vars_to_reduce>0) {
        vars_to_reduce = (int *) malloc( counter_vars_to_reduce * sizeof(int) );
	counter_k=0;
	counter_kk=0;
        for (k = amigo_problem->nx; k < exp1->test.bench.dim; k++){
        	if (x[k]==1) {
        	        vars_to_reduce[counter_k]=counter_kk;
			counter_k++;
        	}
		counter_kk++;
	}
	for (k = 0;  k < amigo_problem->n_models; k++){
		for (j=0;j<counter_vars_to_reduce;j++){
			amigo_problem->amigo_models[k]->pars[amigo_problem->index_inputs[ vars_to_reduce[j] ]]= -1.0;
		}
	}
	free(vars_to_reduce);
    }

    negativo = 0;
    for ( j=0;j<amigo_problem->n_pars;j++) {
	if (((int) amigo_problem->amigo_models[0]->pars[j]) == -1 ) {
		negativo++;
	}	
    }    
    delete_edge=-1; 
    get_reduced_model(amigo_problem, delete_edge);
    k = amigo_problem->active_pars;
    
    res = eval_AMIGO_problem_LSQ(amigo_problem);
    res2 = res;
    if (res == INFINITY) res= DBL_MAX;
    if (isnan(res)) res=DBL_MAX;


    RMSE = sqrt(res/amigo_problem->n_dataR);
    res = 2.0*((double)k) + amigo_problem->n_dataR * log(res);

   // printf("fx=%lf + res=%lf + RMSE=%lf + num neg=%d + k= %d + estimated_pars %d \n", res, res2, RMSE, negativo, k, amigo_problem->estimated_pars);

    // REESTABLECEMENTO
    for (i = 0;  i < amigo_problem->n_models; i++){
        for ( j = 0; j < amigo_problem->n_pars; j++) {
                amigo_problem->amigo_models[i]->pars[j]=x_pars[i][j];
        }
	free(x_pars[i]);
    }
    free(x_pars);
  
    return res;
}
