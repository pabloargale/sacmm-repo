#include <amigoRHS.h>
#include <AMIGO_model.h>
#include <math.h>


int amigoRHS_HEPG2a(realtype , N_Vector , N_Vector , void *);

void amigoRHS_get_OBS_HEPG2a(void* );

void amigoRHS_get_sens_OBS_HEPG2a(void* );

void amigo_Y_at_tcon_HEPG2a(void* , realtype , N_Vector );
